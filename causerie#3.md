# quelques choses croisées pendant la causerie 

* [_TAZ_](http://www.lyber-eclat.net/lyber/taz.html) (Temporary Autonomous Zone, ou Zone Autonome Temporaire) de Hakim Bey 
* autour de la hiérarchie dans le collectif, Frédéric Laloux avec _Reinventing organizations_ 
* le livre de Sébastien Broca [_Utopie du logiciel libre_] (http://lepassagerclandestin.fr/catalogue/essais/utopie-du-logiciel-libre.html)
* les travaux et recherches de Dominique Cardon, notamment une conférence : [_Nos vies à l'heure du Big Data_] (https://www.youtube.com/watch?v=D_Eyzt2uJZk)  un entretien [_Internet et la surveillance_](https://vimeo.com/120570445)  ainsi que _La démocratie Internet_

